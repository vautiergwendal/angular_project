import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path :'' , redirectTo : '/login/sign-in' , pathMatch: 'full'},
  { path :'login' , loadChildren : () => import('./login/login.module').then(m => m.LoginModule)},
  { path :'customers' , loadChildren : () => import('./customers/customers.module').then(m => m.CustomersModule)},
  { path :'orders' , loadChildren : () => import('./orders/orders.module').then(m => m.OrdersModule)},
  { path :'404-not-found' , loadChildren : () => import('./page-not-found/page-not-found.module').then(m => m.PageNotFoundModule)},
  { path :'**' , redirectTo : '/404-not-found' , pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
