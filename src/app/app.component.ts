import { Component } from '@angular/core';
import { VersionService } from './core/services/version.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'EFTL_angular_Gwen';

  constructor(private versionService: VersionService,
    )
    {  }
  
  onNav(label :string): void {
    console.log(label);
  }
  /* incrementVersion(arg:string){
    switch (arg) {
      case "major":
        this.versionService.incrementVersionMajor();
        break;
      case "minor":
        this.versionService.incrementVersionMinor();
        break;
      case "patch":
        this.versionService.incrementVersionPatch();
        break;
      default:
        break;
    }
  } */
  incrementVersion(props: 'major' | 'minor' | 'patch'):void {
    this.versionService.incrementVersion(props);
  }

}
