import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PageListCustomersComponent } from './pages/page-list-customers/page-list-customers.component';
import { PageEditCustomerComponent } from './pages/page-edit-customer/page-edit-customer.component';
import { PageAddCustomerComponent } from './pages/page-add-customer/page-add-customer.component';


@NgModule({
  declarations: [
    PageListCustomersComponent,
    PageEditCustomerComponent,
    PageAddCustomerComponent
  ],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    SharedModule
  ],
  exports:[
    PageListCustomersComponent,
    PageEditCustomerComponent,
    PageAddCustomerComponent
  ]
})
export class CustomersModule { }
