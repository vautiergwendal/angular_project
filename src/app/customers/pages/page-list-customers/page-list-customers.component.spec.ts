import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageListCustomersComponent } from './page-list-customers.component';

describe('PageListCustomersComponent', () => {
  let component: PageListCustomersComponent;
  let fixture: ComponentFixture<PageListCustomersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageListCustomersComponent]
    });
    fixture = TestBed.createComponent(PageListCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
