import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAddCustomerComponent } from './page-add-customer.component';

describe('PageAddCustomerComponent', () => {
  let component: PageAddCustomerComponent;
  let fixture: ComponentFixture<PageAddCustomerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageAddCustomerComponent]
    });
    fixture = TestBed.createComponent(PageAddCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
