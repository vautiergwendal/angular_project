import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEditCustomerComponent } from './page-edit-customer.component';

describe('PageEditCustomerComponent', () => {
  let component: PageEditCustomerComponent;
  let fixture: ComponentFixture<PageEditCustomerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageEditCustomerComponent]
    });
    fixture = TestBed.createComponent(PageEditCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
