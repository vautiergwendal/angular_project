import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageListOrdersComponent } from './pages/page-list-orders/page-list-orders.component';
import { PageEditOrderComponent } from './pages/page-edit-order/page-edit-order.component';
import { PageAddOrderComponent } from './pages/page-add-order/page-add-order.component';

const routes: Routes = [
  {path :'' , redirectTo : '/orders//list' , pathMatch: 'full'},
  {path: 'list', component : PageListOrdersComponent},
  {path: 'add', component : PageAddOrderComponent},
  {path: 'edit/:id', component : PageEditOrderComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
