import { Component, Version } from '@angular/core';
import { Observable, map } from 'rxjs';
import { VersionService } from '../../services/version.service';

@Component({
  selector: 'app-footer',
  template: 'Version : {{version$ | async}}',
 
})
export class FooterComponent{

  //version$:Observable<string>=this.versionService.version$.pipe(map(value => 'v'+ value.major + '.' + value.minor + '.' + value.patch));
  version$:Observable<string>=this.versionService.version$.pipe(map(value => `v${value.major}.${value.minor}.${value.patch}`));

  constructor(private versionService: VersionService){
  }
}
