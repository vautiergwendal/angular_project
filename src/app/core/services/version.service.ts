import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Version } from '../models/version';

@Injectable({
  providedIn: 'root'
})

export class VersionService implements Version{

  private version: BehaviorSubject<Version> = new BehaviorSubject<Version>({major:1,minor:0,patch:0});
  major: number =this.version.value.major;
  minor: number =this.version.value.minor;
  patch: number =this.version.value.patch;

  
  

  get version$() : Observable<Version> {
    return this.version.asObservable();
  }

  constructor() {

  }
  
  incrementVersionMajor(){
    const vversion=this.version.getValue();
    vversion.major = this.version.value.major+1;
    this.version.next(vversion);
  }
  incrementVersionMinor(){
    const vversion=this.version;
    vversion.value.minor = this.version.value.minor+1;
    this.version.next(vversion.value);
  }
  incrementVersionPatch(){
    const vversion=this.version;
    vversion.value.patch = this.version.value.patch-1;
    this.version.next(vversion.value);
  }
  /* incrementVersionMinor(){
    const oldVersion: Version = this.version.getValue();
    const newVersion: Version = {
      ...oldVersion,
      minor: oldVersion.minor+1;
    }
  } */
  /* incrementVersionMinor(){
      this.version.next({
        ...this.version.getValue(),
        patch: this.version.getValue().patch+1;,
      });
    }
  } */
  incrementVersion(props: 'major' | 'minor' | 'patch') : void{
    this.version.next({
      ...this.version.getValue(),
      [props]: this.version.getValue()[props]+1,
    });
  }
}

