import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconsModule } from '../icons/icons.module';
import { LoginModule } from '../login/login.module';
import { UiModule } from '../ui/ui.module';
import { NavComponent } from './components/nav/nav.component';
import { HeaderComponent } from './components/header/header.component';
import { Header2Component } from './components/header2/header2.component';
import { Nav2Component } from './components/nav2/nav2.component';



@NgModule({
  declarations: [
    NavComponent,
    HeaderComponent,
    Header2Component,
    Nav2Component
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IconsModule,
    LoginModule,
    UiModule,
    HeaderComponent,
    NavComponent,
    Header2Component,
    Nav2Component
  ]
})
export class CoreModule { }
