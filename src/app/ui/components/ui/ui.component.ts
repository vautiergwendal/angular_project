import { Component } from '@angular/core';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.scss']
})
export class UiComponent {
  afficher = true;
  toggle(){
    this.afficher = !this.afficher;
    console.log("VU ",this.afficher);
  }
}
