import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSignUPComponent } from './page-sign-up.component';

describe('PageSignUPComponent', () => {
  let component: PageSignUPComponent;
  let fixture: ComponentFixture<PageSignUPComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageSignUPComponent]
    });
    fixture = TestBed.createComponent(PageSignUPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
