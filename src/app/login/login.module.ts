import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { PageForgotPasswordComponent } from './pages/page-forgot-password/page-forgot-password.component';
import { PageResetPasswordComponent } from './pages/page-reset-password/page-reset-password.component';
import { PageSignUPComponent } from './pages/page-sign-up/page-sign-up.component';
import { PageSignInComponent } from './pages/page-sign-in/page-sign-in.component';
import { SharedModule } from '../shared/shared.module';
import { FooterComponent } from '../core/components/footer/footer.component';


@NgModule({
  declarations: [
    FooterComponent,
    PageForgotPasswordComponent,
    PageResetPasswordComponent,
    PageSignUPComponent,
    PageSignInComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,  // Pour défi : afficher composant
    SharedModule,
  ],
  exports:[
    PageForgotPasswordComponent,
    PageResetPasswordComponent,
    PageSignUPComponent,
    PageSignInComponent
  ]
})
export class LoginModule { }
