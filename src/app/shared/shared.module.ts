import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateModule } from "src/app/template/template.module";
import { IconsModule } from '../icons/icons.module';
import { UiModule } from '../ui/ui.module';
import { NavButtonComponent } from './components/nav-button/nav-button.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    NavButtonComponent
  ],
  imports: [
    CommonModule,
    UiModule,
    RouterModule
  ],
  exports: [
    TemplateModule,
    IconsModule,
    UiModule,
    NavButtonComponent
  ]
})
export class SharedModule { }
