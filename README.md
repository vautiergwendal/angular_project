## Création de la première page

ng g component customers/pages/PageListCustomers

ng g component customers/pages/PageAddCustomer

ng g component customers/pages/PageEditCustomer

ng g component login/pages/PageSignIn

ng g component login/pages/PageSignUP

ng g component login/pages/PageResetPassword

ng g component login/pages/PageForgotPassword

ng g component orders/pages/PageListOrders

ng g component orders/pages/PageAddOrder

ng g component orders/pages/PageEditOrder

ng g component page-not-found/pages/PageNotFound

ng g component core/components/Header2 --export

$ ng g component core/components/Nav2 --export

$ ng g component core/components/Footer2 --export